/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supakit.exammidterm;

/**
 *
 * @author SUPAKIT KONGKAM
 */
public class TestProductService {

    public static void main(String[] args) {
        System.out.println(ProductService.getProducts());
        ProductService.addProduct(new Product("001", "Chocolate", "Kitkat",
                3, 20));
        System.out.println(ProductService.getProducts());
        Product updateProduct = new Product("003", "Honey", "mee", 10, 200);
        ProductService.updateProduct(0, updateProduct);
        System.out.println(ProductService.getProducts());
        ProductService.delProduct(updateProduct);
        System.out.println(ProductService.getProducts());
        ProductService.delProduct(0);
        System.out.println(ProductService.getProducts());
    }
}
