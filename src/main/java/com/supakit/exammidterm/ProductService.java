/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supakit.exammidterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SUPAKIT KONGKAM
 */
public class ProductService implements Serializable{

    private static ArrayList<Product> productList = new ArrayList<>();

    static {
        load();

    }

    // Create (C)
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }

    // Delete (D)
    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }

    //Read (R)
    public static ArrayList<Product> getProducts() {
        return productList;
    }

    //Update (U)
    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }

    //clear
    public static boolean clearProduct() {
        productList.clear();
        save();
        return true;
    }

    public static Product getProduct(int index) {
        return productList.get(index);
    }

    public static int showAmount() {
        int amount = 0;
        for (int i = 0; i < productList.size(); i++) {
            amount += productList.get(i).getAmount();
        }
        return amount;
    }

    public static double showPrice() {
        double price = 0;
        for (int i = 0; i < productList.size(); i++) {
            price += productList.get(i).getPrice();
        }
        return price;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Joseph.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Joseph.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

}
