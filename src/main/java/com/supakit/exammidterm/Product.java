/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supakit.exammidterm;

import java.io.Serializable;

/**
 *
 * @author SUPAKIT KONGKAM
 */
public class Product implements Serializable {

    private String id;
    private String name;
    private String brand;
    private int amount;
    private double price;

    @Override
    public String toString() {
        return " ID : "+id + "      , Name : "+ name + "      , Brand : "+ brand + 
                "        , Amount "+ amount + " Items      , Price : "+price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product(String id, String name, String brand,
            int amount, double price) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.amount = amount;
        this.price = price;
    }

}
